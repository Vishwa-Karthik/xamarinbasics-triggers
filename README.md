## Xamarin Triggers

### Implemented
1. Property Triggers
2. Event Triggers
3. Data Triggers

## Results
<p>
<img src="https://gitlab.com/Vishwa-Karthik/xamarinbasics-triggers/-/raw/master/img1.png" width="200" height="400">
&nbsp;
<img src="https://gitlab.com/Vishwa-Karthik/xamarinbasics-triggers/-/raw/master/img2.png" width="200" height="400">
&nbsp;
<img src="https://gitlab.com/Vishwa-Karthik/xamarinbasics-triggers/-/raw/master/img3.png" width="200" height="400">
</p>
