﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace TriggersBasics.Triggers
{
    public class AgeVerification : TriggerAction<Entry>
    {
        protected override void Invoke(Entry sender)
        {
            var entry = sender as Entry;
            var flag = int.TryParse(entry.Text, out int age);
            entry.BackgroundColor = ((!flag) || (age<19 || age >60)) ? Color.Red : Color.Default;
        }
    }
}
